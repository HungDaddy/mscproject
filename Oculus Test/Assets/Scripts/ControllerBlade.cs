﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerBlade : MonoBehaviour {

    public Transform Blade, LeftHand, RightHand;




    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Blade.position = (LeftHand.position + RightHand.position) / 2;
        Blade.LookAt(RightHand);
        Blade.Rotate(90, 0, 0);
	}
}
