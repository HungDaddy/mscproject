﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetRodEnd : MonoBehaviour {

    TargetRod TM;
	// Use this for initialization
	void Awake () {
        TM = transform.parent.GetComponent<TargetRod>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("RodEnd")) {
            TM.UpdateEndCount(1);
            Debug.Log("touched");
        }
    }
    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("RodEnd")) {
            TM.UpdateEndCount(-1);
            Debug.Log("untouched");
        }
    }
}
