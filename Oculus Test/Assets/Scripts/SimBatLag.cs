﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimBatLag : MonoBehaviour
{

    public Transform RC;

    List<Vector3> posList = new List<Vector3>();
    List<Vector3> angList = new List<Vector3>();

    float moveAng = 0;
    float TopSpeed;

    // Use this for initialization
    void Start() {
        TopSpeed = Settings.instance.LagTopSpeed;
    }

    // Update is called once per frame
    void Update() {

        //transform.position = RC.position;
        //transform.eulerAngles = RC.eulerAngles;

        //float dis = Vector3.Distance(transform.position, RC.position);
        //if (dis < 0.01f) {
        //    transform.position = RC.position;
        //    transform.eulerAngles = RC.eulerAngles;
        //}
        //else {
        //    transform.position = Vector3.MoveTowards(transform.position, RC.position, 0.01f);
        //    transform.eulerAngles = RC.eulerAngles;

        //}

        posList.Add(RC.position);
        angList.Add(RC.eulerAngles);
        float dis = Vector3.Distance(transform.position, posList[0]);
        if (dis < Time.deltaTime* TopSpeed) {
            moveAng = 0;
            //maybe improve
            transform.position = posList[0];
            transform.eulerAngles = angList[0];
            posList.RemoveAt(0);
            angList.RemoveAt(0);
            for (int i = 0; i < posList.Count; i++) {
                dis = Vector3.Distance(transform.position, posList[0]);
                if (dis < Time.deltaTime * TopSpeed) {
                    //maybe improve
                    transform.position = posList[0];
                    transform.eulerAngles = angList[0];
                    posList.RemoveAt(0);
                    angList.RemoveAt(0);
                }
                else {
                    break;
                }
            }


        }
        else {
            transform.position = Vector3.MoveTowards(transform.position, posList[0], Time.deltaTime * TopSpeed);

            if (moveAng == 0) {
                float disRatio = (Time.deltaTime * TopSpeed) / dis;
                float angBetween = Quaternion.Angle(transform.rotation, Quaternion.Euler(angList[0]));
                moveAng = angBetween * disRatio;
                //Quaternion moveAngq = Quaternion.RotateTowards(transform.eulerAngles, angList[0]. ;
            }
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(angList[0]), moveAng);
            //transform.eulerAngles = angList[0];
            //Debug.Log(angList[0]);
        }

    }
}
