﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blade : MonoBehaviour {

    public static Blade instance = null;


    Vector3 previous = new Vector3(0, 0, 0);
    Vector3 direction = new Vector3(0, 0, 0);
    public Material red;
    public Material chrome;

    float speed;
    CapsuleCollider CC;
    public AudioClip AC;
    bool vibrating = false;
    float count = 0;
    float count1 = 0;



    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    // Use this for initialization
    void Start () {
        CC = GetComponent<CapsuleCollider>();
	}
	
	// Update is called once per frame
	void Update () {
        direction = transform.position - previous;
        speed = ((direction).magnitude) / Time.deltaTime;
        direction /= Time.deltaTime;
        previous = transform.position;
        //GetComponent<Rigidbody>().MovePosition(transform.position);

        //if(speed > 5) {
        //    GetComponent<Renderer>().material.color = Color.red;
        //}
        //if (speed < 5) {
        //    GetComponent<Renderer>().material.color = Color.white;
        //}

        UpdateCollider();

        //Debug.Log(speed);
        if (vibrating) {
            count += Time.deltaTime;
            if (count > 0.5f) {
                vibrating = false;
                count = 0;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            Debug.Log(speed);
        }
    }


    void UpdateCollider() {


        //CC.radius = Mathf.Min(6, (0.5f + (speed / 2)));
        //CC.height = Mathf.Min(3, (2 + (speed / 2)));
        CC.radius = Mathf.Min(0.1f, (0.04f + (speed / 100)));
        CC.height = Mathf.Min(1, (0.63f + (speed / 100)));

    }
    public float getSpeed() {
        return speed;
    }

    public Vector3 getDir()
    {
        return direction;
    }

    void OnCollisionEnter(Collision other) {
        //Debug.Log("HIT THING");
    }

    public void afterEffect() {
        
    }
    public void TryVibrate() {
        if (!vibrating) {
            OVRHaptics.LeftChannel.Preempt(new OVRHapticsClip(AC));
            OVRHaptics.RightChannel.Preempt(new OVRHapticsClip(AC));
            OVRHaptics.Process();
            vibrating = true;
            GetComponent<AudioSource>().Play();

        }
    }

}
