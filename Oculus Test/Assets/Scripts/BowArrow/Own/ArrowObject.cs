﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowObject : MonoBehaviour {

    public ArrowAttachManager AAM;

    private bool isFired;
    public bool stopped;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (isFired) {
            transform.LookAt(transform.position + transform.GetComponent<Rigidbody>().velocity);
        }
    }
    public void Fired() {
        isFired = true;
        GetComponent<CapsuleCollider>().enabled = true;
    }

    public void attachThis() {
        //AAM
    }

    public void stop() {
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().isKinematic = true;
        GetComponent<Rigidbody>().freezeRotation = true;
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
        GetComponent<CapsuleCollider>().enabled = false;
        stopped = true;
    }

    //private void OnCollisionEnter(Collision collision) {
    //    isFired = false;
    //    GetComponent<Rigidbody>().useGravity = false;
    //    GetComponent<Rigidbody>().isKinematic = true;
    //    GetComponent<Rigidbody>().freezeRotation = true;
    //    GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
    //    GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
    //    GetComponent<CapsuleCollider>().enabled = false;
    //    Debug.Log(transform.eulerAngles);

    //}

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Bounds")) {
            Destroy(this);
        }
    }
}
