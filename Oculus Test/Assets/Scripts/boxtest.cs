﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boxtest : MonoBehaviour {


    [Range(0, 10)]
    public float speed;

    [Range(0, 360)]
    public float degree;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.localRotation = Quaternion.Euler(degree,0,0);
        transform.localScale = new Vector3(1, 1, 1 + (2 * speed));
        transform.localPosition = new Vector3(0, 
            Mathf.Sin(Mathf.Deg2Rad * degree) *speed, 
            Mathf.Cos(Mathf.Deg2Rad * degree) *-speed);
	}
}
