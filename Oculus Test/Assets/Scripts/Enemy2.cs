﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : MonoBehaviour
{

    public GameObject player;
    int damping = 2;
    public bool isHit = false;
    public bool isDead = false;
    float deathCountDown = 3;
    float stillCount = 0;
    [Range(0,30)]
    public float hitForce;

    public AudioClip AC;
    void Awake() {
        player = GameObject.FindGameObjectWithTag("PlayerTarget");
    }

    private void Update() {
        if (isDead) {
            deathCountDown -= Time.deltaTime;
            if (deathCountDown < 0) {
                Destroy(this.gameObject);
                //PlayerStats.takeEnemy();
            }
        }
        //else if (!isHit) {
        //    Vector3 lookPos = player.transform.position - transform.position;
        //    lookPos.y = 0;
        //    Quaternion rotation = Quaternion.LookRotation(lookPos);
        //    transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);

        //    transform.Translate(0, 0, 0.01f);
        //}
        else if (isHit) {
            if (GetComponent<Rigidbody>().velocity.magnitude < 0.01f) {
                stillCount += Time.deltaTime;
                if (stillCount > 0.1f) {
                    GetComponent<Rigidbody>().isKinematic = true;
                    GetComponent<Rigidbody>().detectCollisions = false;
                    EnemyMaker.instance.CreateEnemy();

                    isDead = true;
                }
            }
        }
    }

    void OnTriggerStay(Collider other) {
        if (other.CompareTag("Blade")) {
            //float hitspeed = other.GetComponent<Blade>().getSpeed() * hitForce;
            Vector3 direction = other.GetComponent<Blade>().getDir();
            //Vector3 contact = other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
            // Vector3 force = new Vector3(hitspeed, hitspeed, hitspeed);
            //GetComponent<Rigidbody>().AddForceAtPosition(force, contact);
            //GetComponent<Rigidbody>().AddExplosionForce(hitspeed, contact, 3);
            GetComponent<Rigidbody>().AddForce(direction * hitForce);
            //Debug.Log(direction);
            OVRHaptics.LeftChannel.Preempt(new OVRHapticsClip(AC));
            OVRHaptics.Process();

            isHit = true;
        }


    }

    //private void OnTriggerEnter(Collider other) {
    //    if (other.CompareTag("PlayerTarget")) {
    //        PlayerStats.takeHealth(1);
    //        PlayerStats.takeEnemy();
    //        Destroy(this.gameObject);
    //    }

    //    if (other.CompareTag("ArrowObject")) {
    //        Vector3 force = other.GetComponent<Rigidbody>().velocity;
    //        GetComponent<Rigidbody>().AddForce(force * 10);
    //        other.GetComponent<ArrowObject>().stop();
    //        Quaternion rot = other.transform.rotation;
    //        other.transform.SetParent(transform);
    //        other.transform.rotation = rot;
    //        isHit = true;

    //    }
    //}

    //private void OnCollisionEnter(Collision collision) {
    //    if (collision.transform.CompareTag("Enemy")) {
    //        if (collision.transform.GetComponent<Enemy>().isHit && !collision.transform.GetComponent<Enemy>().isDead) {
    //            isHit = true;
    //        }
    //    }
    //}

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Bounds") && !isDead) {
            isDead = true;
            Debug.Log("THIS?");
            EnemyMaker.instance.CreateEnemy();

        }
    }

    public void GetHit() {
        //OVRHaptics.LeftChannel.Preempt(new OVRHapticsClip(AC));
        isHit = true;
    }
}
