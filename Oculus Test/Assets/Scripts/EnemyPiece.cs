﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPiece : MonoBehaviour {

    float FSR;
    float MHS;
    EnemySplit ES;
    Rigidbody rb;
    bool hit = false;
    public float isAttachedToArm;
    // Use this for initialization
    void Awake () {
        rb = GetComponent<Rigidbody>();
        ES = transform.root.GetComponent<EnemySplit>();
        if (Settings.instance != null) {
            FSR = Settings.instance.FallSpreadRange;
            MHS = Settings.instance.MinHitSpeed;
        }
    }

    private void Start() {
        FSR = Settings.instance.FallSpreadRange;
        MHS = Settings.instance.MinHitSpeed;
    }


    // Update is called once per frame
    void Update () {
		
	}

    private void OnTriggerStay(Collider other) {
        if (other.CompareTag("Blade")) {
            if (!ES.getParry()) {
                return;
            }
            if (other.GetComponent<Blade>().getSpeed() < MHS)
                return;
            //Debug.Log("HIT BY BLADE");
            // if(rb.velocity.magnitude < other.GetComponent<Blade>().getDir().magnitude)
            //GetComponent<Rigidbody>().velocity = other.GetComponent<Blade>().getDir() * 1.1f;
            GetComponent<Rigidbody>().AddForce(other.GetComponent<Blade>().getDir() * Settings.instance.HitForce);
            //Debug.Log("IS HIT?");
            //GetComponent<Rigidbody>().AddExplosionForce(100, transform.position-other.GetComponent<Blade>().getDir(), 1);
            //other.GetComponent<Blade>().afterEffect();
            other.GetComponent<Blade>().TryVibrate();
            if (!hit) {
                RaycastHit[] hits;
                hits = Physics.RaycastAll(transform.position, Vector3.up, 2);
                ES.Hit();
                transform.parent = transform.parent.transform.parent.transform.parent;
                for (int i = 0; i < hits.Length; i++) {
                    RaycastHit hit = hits[i];
                    if (hit.transform.CompareTag("EnemyPiece")) {
                        hit.transform.GetComponent<EnemyPiece>().ActivateGravity();

                    }
                }
                if(GetComponent<BoxCollider>() != null)
                    GetComponent<BoxCollider>().isTrigger = false;
                else if(GetComponent<CapsuleCollider>() != null)
                    GetComponent<CapsuleCollider>().isTrigger = false;


                rb.useGravity = true;

                hit = true;
            }
        }
    }

    public void afterExplode(Vector3 dir) {
        if (!hit) {
            GetComponent<Rigidbody>().AddExplosionForce(100, transform.position - dir, 1);
            //rb.useGravity = true;
            GetComponent<BoxCollider>().isTrigger = false;
        }
    }

    public void ActivateGravity() {
        rb.useGravity = true;
        GetComponent<Rigidbody>().AddForce(Random.Range(-FSR, FSR), 0,Random.Range(-FSR, FSR)) ;
    }

}
