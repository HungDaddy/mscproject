﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour {

    bool hasWon;

    public static int playerHealth;
    public static int enemiesLeft;
    public static GameObject enemies;
    public static TextMesh billboard;
    public static EnemyManager EM;
    public static GameObject Arrows;
    public static bool stopped;
    // Use this for initialization
    void Start () {
        playerHealth = 3;
        enemiesLeft = 20;
        billboard = GameObject.FindGameObjectWithTag("Billboard").GetComponent<TextMesh>();
        enemies = GameObject.FindGameObjectWithTag("Enemies").gameObject;
        EM = GameObject.FindGameObjectWithTag("EnemyManager").GetComponent<EnemyManager>();
        Arrows = GameObject.FindGameObjectWithTag("Arrows");
        updateBillboard();
        stopped = false;
        //lose();
	}

    private static void updateBillboard()
    {
        billboard.text = "Health: " + playerHealth + "\nEnemies: " + enemiesLeft;
    }

    public static void takeHealth(int damage)
    {
        playerHealth -= damage;
        updateBillboard();
        if(playerHealth == 0)
        {
            lose();
            foreach (Transform child in enemies.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
    }

    public static void takeEnemy()
    {
        if (stopped)
            return;
        //Debug.Log("enemy");
        enemiesLeft -= 1;
        updateBillboard();
        if (enemiesLeft == 0)
        {
            win();
            foreach (Transform child in enemies.transform)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
    }

    static void win()
    {
        billboard.text = "You Win! \n Press A to play again!";
        stopped = true;
        EM.stop();
    }

    static void lose()
    {
        Debug.Log("WIN?");
        EM.stop();

        billboard.text = "You Lose! \n Press A to play again!";
        stopped = true;
    }

    // Update is called once per frame
    void Update () {
        if (OVRInput.GetDown(OVRInput.RawButton.A) || OVRInput.GetDown(OVRInput.RawButton.X)) {
            foreach (Transform child in Arrows.transform) {
                Destroy(child.gameObject);
            }
            tryReset();

        }
    }
    public static void tryReset()
    {
        if (stopped)
        {
            EM.ResetEnemies();
            stopped = false;
            playerHealth = 3;
            enemiesLeft = 20;
            updateBillboard();
            foreach(Transform child in Arrows.transform) {
                Destroy(child);
            }
        }
    }
}
