﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

    public Transform spawnPoint;
    public Transform center;
    float counter = 0;
    int enemyCount = 0;
    bool active = true;
    // Update is called once per frame
    void Update () {
        counter += Time.deltaTime;
        if(counter > 2f && enemyCount < 20 && active)
        {
            enemyCount++;
            spawnEnemy();
            counter = 0;
        }
	}

    public void spawnEnemy() {
        float angle = ((int)Random.Range(0, 3) - 1) * 90;
        center.localEulerAngles = new Vector3(0, angle, 0);
        GameObject enemy = Instantiate(Resources.Load("Enemy")) as GameObject;
        enemy.transform.position = spawnPoint.position;
        enemy.transform.Rotate(0, 180 + angle, 0);
        enemy.transform.Translate(Random.Range(-3,3), 0, 0);
        enemy.transform.SetParent(transform.Find("Enemies").transform);
    }

    public void ResetEnemies()
    {
        active = true;
        enemyCount = 0;
    }

    public void stop()
    {
        active = false;
    }
}
