﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager1 : MonoBehaviour {
    public static EnemyManager1 instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.

    public Transform spawnPoint;
    public Transform center;
    float counter = 0;
    float SpawnFreq;
    int enemyCount = 0;
    bool active = false;

    //string[] EnemyTypes = new string[] {"EnemySplit","Enemy};

    private void Awake() {
        if(instance == null) {
            instance = this;
        }
    }

    private void Start() {
        SpawnFreq = Settings.instance.EnemySpawnFreq;
    }

    // Update is called once per frame
    void Update () {
        counter += Time.deltaTime;
        if(counter > SpawnFreq 
            && (enemyCount < Settings.instance.EnemySpawns /*|| Settings.instance.EnemySpawns == 0*/)
            && active)
        {
            enemyCount++;
            spawnEnemy();
            counter = 0;
            if (Settings.instance.FasterFreq) {
                SpawnFreq -= Settings.instance.EnemySpawnFreqChange;
                if (SpawnFreq < Settings.instance.EnemySpawnFreqChange) {
                    SpawnFreq = Settings.instance.EnemySpawnFreqChange;
                }
            }
        }
	}

    public void spawnEnemy() {
        //float angle = ((int)Random.Range(0, 3) - 1) * 90;
        float angle = Random.Range(-80, 80) ;
        int type = (int)Random.Range(0, 4);
        center.localEulerAngles = new Vector3(0, angle, 0);
        string s = "";
        if (type == 0)
            s = "EnemySplit";
        else if(type == 1) {
            s = "EnemySplit Flipped";
        }
        else if (type == 2) {
            s = "EnemySplit Sword";
        }
        else if (type == 3) {
            s = "EnemySplit Flipped Sword";
        }
        GameObject enemy = Instantiate(Resources.Load(s)) as GameObject;
        enemy.transform.position = spawnPoint.position;
        //enemy.transform.Rotate(0, angle, 0);
        //enemy.transform.Translate(Random.Range(-3,3), 0, 0);
        enemy.transform.SetParent(transform.Find("Enemies").transform);
    }

    public void ResetEnemies()
    {
        SpawnFreq = Settings.instance.EnemySpawnFreq;

        active = true;
        enemyCount = 0;
        counter = 0;
    }

    public void SetActive(bool b) {
        active = b;

        if (!b) {
            foreach(Transform c in transform.Find("Enemies").transform) {
                Destroy(c.gameObject);
            }
        }
    }
}
