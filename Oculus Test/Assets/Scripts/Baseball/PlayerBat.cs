﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBat : MonoBehaviour {

    public GameObject Bat;
    public GameObject OneCont;
    public GameObject TwoCont;

    public bool RealBat;
    public bool OneController;
    public bool TwoController;

    public static string mode;

    // Use this for initialization
    void Start () {
        if (RealBat) {
            Bat.SetActive(true);
            mode = "RealBat";
        }
        else if (OneController) {
            OneCont.SetActive(true);
            mode = "OneController";
        }
        else if (TwoController) {
            TwoCont.SetActive(true);
            mode = "TwoController";
        }

    }

    // Update is called once per frame
    //void Update() {
    //    OVRInput.Update();

    //    if (OVRInput.GetDown(OVRInput.RawButton.A)) {
    //        Debug.Log("FHAEU");
    //    }
    //}
}
