﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    //int damping = 2;
    public bool isHit = false;
    public bool isDead = false;
    float deathCountDown = 3;
    float stillCount = 0;
    void Awake() {
        //player = GameObject.FindGameObjectWithTag("PlayerTarget");
    }

    private void Update() {
        if (isDead) {
            deathCountDown -= Time.deltaTime;
            if (deathCountDown < 0) {
                Destroy(this.gameObject);
                //PlayerStats.takeEnemy();
            }
        }
        else if (!isHit) {
            transform.Translate(0, 0, 0.01f);
        }
        else if (isHit) {
            if (GetComponent<Rigidbody>().velocity.magnitude < 0.01f) {
                stillCount += Time.deltaTime;
                if (stillCount > 0.1f) {
                    GetComponent<Rigidbody>().isKinematic = true;
                    GetComponent<Rigidbody>().detectCollisions = false;

                    isDead = true;
                }
            }
        }
    }

    void OnTriggerStay(Collider other) {
        if (other.CompareTag("Bat")) {
            //float hitspeed = other.GetComponent<Bat>().getSpeed() * 100;
            Vector3 direction = other.GetComponent<Bat>().getDir();
            //Vector3 contact = other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
            //Vector3 force = new Vector3(hitspeed, hitspeed, hitspeed);
            //GetComponent<Rigidbody>().AddForceAtPosition(force, contact);
            //GetComponent<Rigidbody>().AddExplosionForce(hitspeed, contact, 3);
            GetComponent<Rigidbody>().AddForce(direction * 100);
            GetComponent<Rigidbody>().useGravity = true;
            GetComponent<Rigidbody>().isKinematic = false;
            //Debug.Log(direction);

            isHit = true;
        }


    }


    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Bounds")) {
            isDead = true;
        }
    }

    public void GetHit() {
        isHit = true;
    }
}