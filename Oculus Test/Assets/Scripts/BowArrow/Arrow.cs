﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {


    bool isAttached = false;
    bool isFired = false;

    private void OnTriggerEnter(Collider other)
    {
        AttachArrow();
    }

    private void OnTriggerStay(Collider other)
    {
        AttachArrow();
    }

    private void Update() {
        if (isFired) {
            transform.LookAt(transform.position + transform.GetComponent<Rigidbody>().velocity);
        }
    }

    public void Fired() {
        isFired = true;
    }

    private void AttachArrow()
    {
        //var device = SteamVR_Controller.Input((int)ArrowManager.Instance.trackedObj.index);
        //float triggerAxis = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x;
        //float value = 1 - triggerAxis;
        //if (!isAttached && value < 0.5f)
        //{
        //    Debug.Log(value);
        //    ArrowManager.Instance.AttachBowToArrow();
        //}
    }
}
