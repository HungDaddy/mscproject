﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMaker : MonoBehaviour {

    public GameObject enemy;

    public static EnemyMaker instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
    Transform mc;
    float count = 0;
    bool done = false;
    // Use this for initialization
    void Awake () {
        if (instance == null)

            //if not, set instance to this
            instance = this;

        mc = GameObject.Find("Main Camera").transform;
    }
	
	// Update is called once per frame
	void Update () {

        count += Time.deltaTime;
        if (!done && count > 5) {
            done = true;
            CreateEnemy();
        }
    }

    public void CreateEnemy() {
        GameObject e = Instantiate(enemy) as GameObject;
        e.transform.position = new Vector3(mc.position.x, 0.35f, mc.position.z + 0.75f);
    }
}
