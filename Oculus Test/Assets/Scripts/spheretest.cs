﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spheretest : MonoBehaviour {

    public Vector3 teleportPoint;
    public Rigidbody rb;

    void Start() {
        rb = GetComponent<Rigidbody>();
        teleportPoint = new Vector3(-1.1f, 0.76f, 0.15f);
        Invoke("Move", 2);
    }

    void FixedUpdate() {
        //rb.MovePosition(teleportPoint);
    }
    void Move() {
        rb.MovePosition(teleportPoint);
    }
}
