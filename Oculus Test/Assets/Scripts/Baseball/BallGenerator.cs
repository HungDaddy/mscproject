﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGenerator : MonoBehaviour {
    public Transform spawnPoint;
    float counter = 0;
    int ballCount = 0;
    bool active = true;
    bool audioPlayed = false;
    AudioSource aud;
    // Update is called once per frame

    void Awake() {
        aud = GetComponent<AudioSource>();   
    }
    void Update() {
        counter += Time.deltaTime;
        if(counter > 2.8f && ballCount < 100 && active && !audioPlayed) {
            aud.Play();
            audioPlayed = true;
        }
        if (counter > 3f && ballCount < 100 && active) {
            ballCount++;
            spawnBall();
            counter = 0;
            audioPlayed = false;
        }
    }

    public void spawnBall() {
       // float angle = ((int)Random.Range(0, 3) - 1) * 90;
        GameObject ball = Instantiate(Resources.Load("Ball 1")) as GameObject;
        ball.transform.position = spawnPoint.position;
        ball.transform.Rotate(0, 180, 0);
        //ball.transform.Translate(Random.Range(-3, 3), 0, 0);
        ball.transform.SetParent(transform.Find("Balls").transform);
    }

    public void ResetEnemies() {
        active = true;
        ballCount = 0;
    }

    public void stop() {
        active = false;
    }
}
