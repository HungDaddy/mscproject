﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bow : MonoBehaviour {

    public Transform rightController;
    public Transform leftController;

    //Vector3 defaultRotation = new Vector3(20.28f, 0, 0);
    bool arrowAttached = false;
    public static Bow instance = null;

    // Use this for initialization
    void Awake () {

        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
    }

    private void Start() {
        //setArrowAttached(true);
    }

    // Update is called once per frame
    void FixedUpdate () {
        if (arrowAttached) {
            Vector3 dir = leftController.position - rightController.position;

            var rotation = Quaternion.LookRotation(dir);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 30);
            //transform.rotation = rotation;
            transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, leftController.localEulerAngles.z);
            transform.position = leftController.position;

            if (Input.GetKeyDown(KeyCode.A)) {
                Debug.Log(rotation);
            }
        }
        else {
            Quaternion rot = leftController.rotation;
            rot.eulerAngles = new Vector3(rot.eulerAngles.x + 20.28f, rot.eulerAngles.y, rot.eulerAngles.z);
            transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * 30);
        }



    }

    public void setArrowAttached(bool b) {
        arrowAttached = b;
        if (b) {
            transform.SetParent(leftController.parent.transform);
        }
        else {
            transform.SetParent(leftController);
        }
    }
}
