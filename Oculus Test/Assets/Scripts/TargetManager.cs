﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetManager : MonoBehaviour {
    public int score = 0;
    public float time;
    public bool gameOn = false;
    public TextMesh text;
    public GameObject TR;
    public Transform cam;
    // Use this for initialization
    void Start () {
        gameOn = false;
        text.text = "Push x to Start";

    }
	
	// Update is called once per frame
	void Update () {
        if (gameOn) {
            time -= Time.deltaTime;
            text.text = "Time:" + (int)time + "\nScore: " + score;
            if (time <= 0) {
                gameOn = false;
                text.text = "Time's Up! " + "\nScore: " + score + "\nPush X to try again";
                TR.GetComponent<TargetRod>().touchCount = 0;
                TR.GetComponent<TargetRod>().UpdateEndCount(0);
                TR.SetActive(false);
            }

        }

        OVRInput.Update();

        if (!gameOn && OVRInput.GetDown(OVRInput.RawButton.X)) {
            transform.position = cam.position;
            transform.Translate(0, -0.3f, 0.55f);
            gameOn = true;
            TR.SetActive(true);
            TR.transform.position = transform.position;
            time = 31;
            score = 0;
        }
    }

    public void UpdateScore(int n) {
        score += n;
    }
}
