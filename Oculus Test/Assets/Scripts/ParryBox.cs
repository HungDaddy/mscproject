﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParryBox : MonoBehaviour {

    EnemySword sword;
    AudioSource AC;
    // Use this for initialization
    void Awake() {
        sword = transform.parent.GetComponent<EnemySword>();
    }

    // Update is called once per frame
    void Update() {

    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Blade")) {
            sword.Parry(true);
            this.gameObject.SetActive(false);
        }
    }
}
