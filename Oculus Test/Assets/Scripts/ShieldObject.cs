﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldObject : MonoBehaviour {

    Animator Anim;
    public AudioSource AC;
    public AudioClip BlockAC;
    public AudioClip PowerOnAC;
	// Use this for initialization
	void Awake () {
        Anim = GetComponent<Animator>();
        AC.clip = PowerOnAC;
        AC.Play();
    }
	
	// Update is called once per frame
	void Update () {

	}

    public void Parry() {
        Anim.SetTrigger("Parried");
        
    }
    public void Unparry() {
        AC.clip = PowerOnAC;
        AC.Play();
        Anim.SetTrigger("Unparry");
    }
    public void Block() {
        AC.clip = BlockAC;
        AC.Play();
        Anim.SetTrigger("Blocked");
    }
}
