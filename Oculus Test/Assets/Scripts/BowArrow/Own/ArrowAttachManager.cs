﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowAttachManager : MonoBehaviour {

    public GameObject ArrowPrefab;
    public GameObject Arrows;
    public Transform ArrowHoldPoint;

    public Transform defaultArrowPoint;
    public Transform GoldenBow;
    public Transform String;
    public Transform defaultStringPoint;
    private float defaultStringPointx;

    public Transform rightController;
    public Transform leftController;

    private float defaultMagnitude;

    private bool arrowAttached;

    GameObject currentArrow = null;
    Vector3 startAttachPos;

    // Use this for initialization
    void Awake () {
        defaultStringPointx = defaultStringPoint.localPosition.x;
	}
	
	// Update is called once per frame
	void Update () {
        if (arrowAttached) {
            OVRInput.Update();

            float currentMagnitude = (leftController.position - rightController.position).magnitude;
            float difMag = currentMagnitude - defaultMagnitude;
            //Debug.Log(difMag);
            String.localPosition = new Vector3(defaultStringPointx + (difMag*10), 0, 0);

            if (OVRInput.GetUp(OVRInput.RawButton.RIndexTrigger)) {
                Fire(difMag);
                
            }
        }
    }

    private void Fire(float dist) {
        currentArrow.transform.parent = Arrows.transform;
        currentArrow.GetComponent<ArrowObject>().Fired();
        Rigidbody r = currentArrow.GetComponent<Rigidbody>();
        r.useGravity = true;
        r.isKinematic = false;
        r.velocity = currentArrow.transform.forward * 30f * dist;

        String.localPosition = defaultStringPoint.localPosition;
        currentArrow = null;
        arrowAttached = false;
        Bow.instance.setArrowAttached(false);

        GameObject arrow = Instantiate(ArrowPrefab);
        arrow.transform.SetParent(rightController);
        arrow.transform.SetPositionAndRotation(ArrowHoldPoint.position, rightController.rotation);
        arrow.transform.localScale = ArrowHoldPoint.localScale;
    }

    private void OnTriggerStay(Collider other) {
        OVRInput.FixedUpdate();
        //if (OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger)) {
        //    Debug.Log("JDUIOAWJ");
        //}
        if (OVRInput.GetDown(OVRInput.RawButton.RIndexTrigger) && other.CompareTag("ArrowAttach") && !arrowAttached) {
            other.transform.parent.transform.SetParent(String);
            other.transform.parent.transform.localPosition = defaultArrowPoint.localPosition;
            other.transform.parent.transform.localRotation = defaultArrowPoint.localRotation;
            arrowAttached = true;
            Bow.instance.setArrowAttached(true);
            currentArrow = other.transform.parent.gameObject;
            defaultMagnitude = (leftController.position - rightController.position).magnitude;
            Destroy(other.gameObject);
        }
    }



}
