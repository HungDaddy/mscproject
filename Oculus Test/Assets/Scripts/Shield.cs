﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour {

    public GameObject lookObj;
    public EnemySplit ES;
    public ShieldObject SO;
    public bool isBlocking;

    // Use this for initialization
    void Awake () {
        lookObj = GameObject.FindGameObjectWithTag("Blade");
        isBlocking = true;
	}
	
	// Update is called once per frame
	void Update () {


        //if (isBlocking) { 
        //    transform.LookAt(lookObj.transform);

        //    float batYPos = lookObj.transform.position.y;
        //    if (batYPos > 1) {
        //        transform.localPosition = new Vector3(0, 1, 0);
        //    }
        //    else if (batYPos < 0.4f) {
        //        transform.localPosition = new Vector3(0, 0.4f, 0);

        //    }
        //    else {
        //        transform.localPosition = new Vector3(0, batYPos, 0);
        //    }
        //}
    }

    private void OnTriggerEnter(Collider other) {
        if(!ES.getParry() && other.CompareTag("Blade")) {
            SO.Block();
        }
    }

    public void parry(bool b) {

    }
}
