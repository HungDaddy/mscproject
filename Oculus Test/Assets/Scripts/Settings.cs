﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour {

    public static Settings instance = null;

    public int PlayerHealth;
    public float FaceHeight;
    public float StartOrbTime;

    public int EnemySpawns;
    public float EnemySpawnFreq;

    public bool FasterFreq;
    public float EnemySpawnFreqChange;

    public float EnemySpeed;
    public float EnemyMinDist;
    public float EnemyMaxDist;

    public float MinHitSpeed;
    public float HitForce;
    public float FallSpreadRange;

    public float LagTopSpeed;



    // Use this for initialization
    void Awake () {
		if(instance == null) {
            instance = this;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
