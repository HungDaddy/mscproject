﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySplit : MonoBehaviour {

    GameObject player;
    public GameObject sword;
    public GameObject EnemyObject;
    public ShieldObject Shield;
    int damping = 2;
    bool isHit = false;
    bool isParried = false;
    public bool justSword;
    float EMAXD;
    float EMIND;

    // Use this for initialization
    void Start () {
        player = GameObject.FindGameObjectWithTag("MainCamera");
        EnemyObject = transform.Find("EnemyObject").gameObject;
        //sword = transform.Find("EnemyObject").transform.Find("Sword").gameObject;
        EMAXD = Settings.instance.EnemyMaxDist;
        EMIND = Settings.instance.EnemyMinDist;
        if (justSword) {
            isParried = true;
        }
    }

    // Update is called once per frame
    void Update () {

        if (isHit)
            return;

        Vector3 lookPos = player.transform.position - EnemyObject.transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        EnemyObject.transform.rotation = Quaternion.Slerp(EnemyObject.transform.rotation, rotation, Time.deltaTime * damping);
        if (Vector3.Distance(EnemyObject.transform.position, player.transform.position) > EMAXD)
            EnemyObject.transform.Translate(0, 0, Settings.instance.EnemySpeed * Time.deltaTime);
        else if (Vector3.Distance(EnemyObject.transform.position, player.transform.position) < EMIND)
            EnemyObject.transform.Translate(0, 0, -Settings.instance.EnemySpeed * Time.deltaTime);
        else
            sword.GetComponent<Animator>().SetTrigger("Hit");

    }

    public void Hit() {
        if (!isHit)
            Invoke("Die", 5);
        isHit = true;
        sword.GetComponent<Animator>().enabled = false;
        transform.Find("EnemyObject").transform.Find("Sword").transform.Find("Hitbox").gameObject.SetActive(false);

    }

    void Die() {
        Player.instance.EnemyDead();
        Destroy(this.gameObject);
    }

    public void setParry(bool b) {
        if (justSword) {
            return;
        }
        isParried = b;
        if (b) {
            Shield.Parry();
        }
        else {
            Shield.Unparry();
        }
    }

    public bool getParry() {
        return isParried;
    }
}
