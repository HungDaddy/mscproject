﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

    public static Player instance = null;              //Static instance of GameManager which allows it to be accessed by any other script.
    public int health;
    public int enemiesLeft;
    public TextMesh text;
    public bool GameRunning;
    public Material green;
    public Material white;
    public GameObject StartOrb;
    public GameObject Maincam;
    ScreenFade sf;
    Vector3 CalibVector;
    public AudioSource AC;

    // Use this for initialization
    void Awake () {
        if (instance == null) {

            //if not, set instance to this
            instance = this;
        }



        text.text = "Touch the orb to start";
        GameRunning = false;
        sf = Maincam.GetComponent<ScreenFade>();
    }
    private void Start() {
        CalibVector = new Vector3(0, Settings.instance.FaceHeight, -2.64f);
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Space)) {
        }
	}

    public void Damage(int dmg) {
        health -= dmg;
        text.text = "Health: " + health + "\nEnemies: " + enemiesLeft;
        sf.Fade(FadeType.In);
        AC.Play();
        if(health <= 0) {
            text.text = "You Lose!\nTouch the Orb to Try Again";
            EndGame();
        }
    }

    public void EnemyDead() {
        enemiesLeft--;
        text.text = "Health: " + health + "\nEnemies: " + enemiesLeft;
        if (enemiesLeft <= 0) {
            text.text = "You Win!\nTouch the Orb to Try Again";
            EndGame();
        }
    }

    public void StartGame() {
        GameRunning = true;
        StartOrb.SetActive(false);
        float yRotDif = Maincam.transform.eulerAngles.y;
        transform.Rotate(0, -yRotDif, 0);
        Vector3 dif = Maincam.transform.position - CalibVector;
        transform.Translate(-dif, Space.World);
        enemiesLeft = Settings.instance.EnemySpawns;
        health = Settings.instance.PlayerHealth;
        text.text = "Health: " + health + "\nEnemies: " + enemiesLeft;
        EnemyManager1.instance.ResetEnemies();
    }

    public void EndGame() {
        GameRunning = false;
        EnemyManager1.instance.SetActive(false);
        StartOrb.SetActive(true);
    }

}
