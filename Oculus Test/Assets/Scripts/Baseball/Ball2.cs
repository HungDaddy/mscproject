﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball2 : MonoBehaviour
{


    public bool isHit = false;
    public bool isDead = false;
    float deathCountDown = 3;
    float stillCount = 0;

    float count = 0;
    void Awake() {
        //player = GameObject.FindGameObjectWithTag("PlayerTarget");
    }
    void Start() {
        //GetComponent<Rigidbody>().AddForce(0, 300, -900);
        GetComponent<Rigidbody>().AddForce(0, 250, -450);
    }

    private void Update() {

        //count += Time.deltaTime;
        if (count > 0.2f) {
            GameObject trailball = Instantiate(Resources.Load("TrailBall")) as GameObject;
            trailball.transform.position = transform.position;
            count = 0;
        }
        if (isDead) {
            deathCountDown -= Time.deltaTime;
            if (deathCountDown < 0) {
                Destroy(this.gameObject);
                //PlayerStats.takeEnemy();
            }
        }
        else if (!isHit) {
            //transform.Translate(0, 0, 0.01f);
            //GetComponent<Rigidbody>().AddForce(0, 4.955f, 0);
        }
        else if (isHit) {
            if (GetComponent<Rigidbody>().velocity.magnitude < 0.01f) {
                stillCount += Time.deltaTime;
                if (stillCount > 0.1f) {
                    GetComponent<Rigidbody>().isKinematic = true;
                    GetComponent<Rigidbody>().detectCollisions = false;

                    isDead = true;
                }
            }
        }
        //byte[] noize = { 255 };
        // tho 40 was about when I coulde start feel it
        //byte[] noize = { 40};

        //outputs vibration to left controller
        //OVRHaptics.LeftChannel.Preempt(new OVRHapticsClip(AC));
        //OVRHaptics.LeftChannel.Preempt(new OVRHapticsClip(noize, 1));
        //OVRHaptics.Process();
    }

    void OnTriggerStay(Collider other) {
        if (other.CompareTag("Bat")) {
            if (!isHit) {
                other.GetComponent<Bat>().Hit();
                //Debug.Log("HIT BAT");
                Hit(other.GetComponent<Bat>().getDir());

            }

        }


    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.transform.CompareTag("Goal")) {
            Debug.Log("GOAL");
        }
    }

    public void Hit(Vector3 direction) {
        //Debug.Log("HIT BAT");
        //float hitspeed = other.GetComponent<Bat>().getSpeed() * 100;
        //Vector3 direction = other.GetComponent<Bat>().getDir();
        //Vector3 contact = other.gameObject.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
        //Vector3 force = new Vector3(hitspeed, hitspeed, hitspeed);
        //GetComponent<Rigidbody>().AddForceAtPosition(force, contact);
        //GetComponent<Rigidbody>().AddExplosionForce(hitspeed, contact, 3);
        if (!isHit) {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            GetComponent<SphereCollider>().isTrigger = false;

            //other.GetComponent<Bat>().Hit();
            //Debug.Log("HIT BAT");

        }

        //GetComponent<Rigidbody>().AddForce(direction * 100);
        GetComponent<Rigidbody>().velocity = direction*3;

        //Debug.Log(direction);

        isHit = true;
    }


    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Bounds")) {
            isDead = true;
        }
    }

    public void GetHit() {
        isHit = true;
    }
}