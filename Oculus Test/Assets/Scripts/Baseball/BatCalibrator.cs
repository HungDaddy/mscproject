﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatCalibrator : MonoBehaviour {

    float count = 0;
    Vector3 lastpos;
	// Use this for initialization
	void Start () {
        lastpos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        count += Time.deltaTime;
		if(count > 2) {
            transform.position = Vector3.Lerp(lastpos, transform.position, 0.5f);
            lastpos = transform.position;
            count = 0;
        }
	}
}
