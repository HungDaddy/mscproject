﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordClash : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("DestroySelf", 1);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void DestroySelf() {
        Destroy(this.gameObject);
    }
}
