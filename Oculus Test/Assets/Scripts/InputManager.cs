﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public Transform MainDisCam;
    public Transform RightController;
    public EnemyManager EM;

    [Range(0, 10)]
    public float camDis;
    // Update is called once per frame
    void Update () {
        OVRInput.Update();

        if (OVRInput.GetDown(OVRInput.RawButton.A) ) {
            MainDisCam.position = RightController.transform.position;
            MainDisCam.Translate(0, 0.5f, -camDis);
        }

        //if (OVRInput.Get(OVRInput.RawButton.RIndexTrigger)) {
        //    EM.spawnEnemy();
        //}

    }
}
