﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spin : MonoBehaviour {

    public float speed = 3;
    int dir = 1;
    float wait = 0;
    float waitAll = 0;
    public bool waiting = false;
    public spin s;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        wait += Time.deltaTime;
        if(wait > 3) {
            waiting = false;
            wait = 0;
        }

        //if (waiting) {
        //    if (s.waiting) {
        //        wait += Time.deltaTime;
        //        if (wait > 3) {
        //            waiting = false;

        //        }
        //    }
        //}
        if(!waiting) {
            transform.Rotate(Vector3.up * speed * Time.deltaTime * dir);
            float y = transform.eulerAngles.y;
            if(y > 180) {
                y -= 360;
            }
            if ((y < -90f && dir == -1)|| (y > 90 && dir ==1)) {
                //Debug.Log(transform.eulerAngles.y);
                waiting = true;
                //wait = 0;
                dir *= -1;
            }
        }

	}
}
