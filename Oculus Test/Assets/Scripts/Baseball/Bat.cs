﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
using UnityEngine.UI;

public class Bat : MonoBehaviour
{
    AudioSource aud;
    public AudioClip AC;

    public TextMesh t;

    Vector3 previous = new Vector3(0, 0, 0);
    Vector3 direction = new Vector3(0, 0, 0);

    Vector3 previousR = new Vector3(0, 0, 0);
    Vector3 directionR = new Vector3(0, 0, 0);

    List<float> speeds = new List<float>();
    List<float> times = new List<float>();

    Vector3[] castPos = new Vector3[13];

    float count = 0;
    float count1 = 0;
    float countR = 0;
    float speed;
    float speedR;
    //bool written = false;
    bool recording = false;
    void Awake() {
        //t = GameObject.Find("BillBoard").transform.GetComponent<Text>();

    }

    // Use this for initialization
    void Start() {
        aud = GetComponent<AudioSource>();
        for(int i = 0; i < 13; i++) {
            castPos[i] = transform.GetChild(i).transform.position;
        }
    }

    // Update is called once per frame
    void Update() {
        count1 += Time.deltaTime;

        direction = transform.position - previous;
        speed = ((direction).magnitude) / Time.deltaTime;
        direction /= Time.deltaTime;
        previous = transform.position;
        if (recording) {
            countR += Time.deltaTime;

            if (countR > 0.025f) {
                directionR = transform.position - previousR;
                speedR = ((directionR).magnitude) / 0.025f;
                speeds.Add(speedR);
                times.Add(count1);
                previousR = transform.position;
                countR = 0;
            }

           // speeds.Add(speed);
           // times.Add(count1);
        }

        for (int i = 0; i < 13; i++) {
            RaycastHit hit;
            Vector3 fromPosition = castPos[i];
            Vector3 toPosition = transform.GetChild(i).transform.position;
            //Vector3 direction = toPosition - fromPosition;


            if (Physics.Linecast(fromPosition, toPosition, out hit)) {
                //Debug.Log(hit.collider.transform.name);
                if (hit.collider.CompareTag("Ball") && hit.collider.GetComponent<Ball2>().isHit == false) {
                    Hit();
                    hit.collider.GetComponent<Ball2>().Hit(getDir());
                    Debug.Log("TRACED");
                }
            }

            castPos[i] = toPosition;

        }

        //Debug.Log(speed);
        //count += Time.deltaTime;
        //if(count > 5 && !written) {
        //    string path = "Assets/Resources/test.txt";

        //    //Write some text to the test.txt file
        //    StreamWriter writer = new StreamWriter(path, true);
        //    foreach(float s in speeds) {
        //        writer.WriteLine(s);
        //    }
        //    writer.Close();
        //    written = true;
        //    //Re-import the file to update the reference in the editor
        //    //AssetDatabase.ImportAsset(path);
        //    //TextAsset asset = Resources.Load("test");
        //}


        if (Input.GetKeyDown(KeyCode.R)) {
            if (!recording) {
                recording = true;
            }
            else if (recording) {
                count = 0;


                while (true) {
                    string path = "Assets/Resources/test" + count + ".txt";

                    if (File.Exists(path)) {
                        count++;
                        continue;
                    }
                    else {

                        StreamWriter writer = new StreamWriter(path, true);
                        //foreach (float s in speeds) {
                        //    writer.WriteLine(s + " " + count1.ToString("n4"));
                        //}

                        for(int i = 0; i < speeds.Count; i++) {
                            writer.WriteLine(speeds[i] + "      " + times[i].ToString("n4"));
                        }
                        writer.Close();
                        break;
                    }

                }
                //written = true;
                recording = false;
                speeds = new List<float>();
            }
        }
    }

    public float getSpeed() {
        return speed;
    }

    public Vector3 getDir() {
        return direction;
    }

    public void Hit() {
        aud.Play();
        t.text = speed.ToString("n2") + " m/s";
        //outputs vibration to left controller
        if(PlayerBat.mode == "OneController") {
            OVRHaptics.RightChannel.Preempt(new OVRHapticsClip(AC));
        }
        else if(PlayerBat.mode == "Realbat"){
            OVRHaptics.LeftChannel.Preempt(new OVRHapticsClip(AC));
        }
        else {
            OVRHaptics.RightChannel.Preempt(new OVRHapticsClip(AC));
            OVRHaptics.LeftChannel.Preempt(new OVRHapticsClip(AC));
        }
        OVRHaptics.Process();
    }
}