﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySword : MonoBehaviour {

    public EnemySplit ES;
    public Shield shield;
    public Transform CP;
    Animator Anim;
    AudioSource AC;
	// Use this for initialization
	void Awake () {
        Anim = GetComponent<Animator>();
        AC = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update () {
		
	}



    public void Parry(bool b) {
        if (b) {
            Anim.SetTrigger("Parry");
            //shield.
            Blade.instance.TryVibrate();
            GameObject clash = Instantiate(Resources.Load("HitEffect")) as GameObject;
            clash.transform.position = CP.position;
            clash.transform.position = new Vector3(CP.position.x, Blade.instance.transform.position.y, CP.position.z);
            AC.Play();
        }

        ES.setParry(b);
    }

    public void iParry(int i) {

        ES.setParry(false);
        

    }
}
