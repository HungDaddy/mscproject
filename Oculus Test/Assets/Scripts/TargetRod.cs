﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetRod : MonoBehaviour {
    public Material transGreen;
    public Material transWhite;
    Renderer r;
    public int touchCount = 0;
    public float countDownTime = 3;
    bool inTarget = false;
    float count = 0;
    public TargetManager TM;
    // Use this for initialization
    void Awake () {
        r = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (inTarget) {
            count += Time.deltaTime;
            if(count > countDownTime) {
                transform.localEulerAngles = new Vector3(0, 0, Random.Range(-180, 180));
                transform.localPosition = new Vector3(Random.Range(-0.3f, 0.3f), Random.Range(-0.2f, 0.2f), 0);           count = 0;
                //touchCount = 0;
                UpdateEndCount(0);
                TM.UpdateScore(1);
            }
        }
	}

    public void UpdateEndCount(int n) {
        touchCount += n;
        if (touchCount == 2) {
            Debug.Log("IN TARGET");
            inTarget = true;
            r.material = transGreen;
        }
        else {
            inTarget = false;
            r.material = transWhite;
            count = 0;
        }
    }
}
