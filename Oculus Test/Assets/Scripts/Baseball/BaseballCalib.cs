﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseballCalib : MonoBehaviour
{

    public Transform MainCam;
    public Transform PlayerStick;
    public Transform CalibPoint;

    // Update is called once per frame
    void Update() {
        OVRInput.Update();

        if (OVRInput.GetDown(OVRInput.RawButton.A) || OVRInput.GetDown(OVRInput.RawButton.X)) {
            Debug.Log("KABLOEE");
            PlayerStick.Translate(CalibPoint.position - MainCam.position);
        }
    }
}