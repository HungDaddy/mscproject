﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartOrb : MonoBehaviour {

    float count;
    bool touching;
    public Material green;
    public Material white;
    Renderer r;

    float StartOrbTime;

    // Use this for initialization
    void Start () {
        count = 0;
        touching = false;
        r = GetComponent<Renderer>();
        StartOrbTime = Settings.instance.StartOrbTime;

    }
	
	// Update is called once per frame
	void Update () {
        if (touching) {
            count += Time.deltaTime;
            if(count > StartOrbTime) {
                touching = false;
                count = 0;
                r.material = white;
                Player.instance.StartGame();
            }
        }
	}

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Blade")) {
            touching = true;
            r.material = green;

        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Blade")) {
            touching = false;
            count = 0;
            r.material = white;
        }
    }
}
